package status

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func OK() error {
	return nil
}

func Canceled(message string) error {
	return status.Errorf(codes.Canceled, message)
}

func Unknown(message string, a ...interface{}) error {
	return status.Errorf(codes.Unknown, message, a...)
}

func InvalidArgument(message string, a ...interface{}) error {
	return status.Errorf(codes.InvalidArgument, message, a...)
}

func DeadlineExceeded(message string, a ...interface{}) error {
	return status.Errorf(codes.DeadlineExceeded, message, a...)
}

func NotFound(message string, a ...interface{}) error {
	return status.Errorf(codes.NotFound, message, a...)
}

func AlreadyExists(message string, a ...interface{}) error {
	return status.Errorf(codes.AlreadyExists, message, a...)
}

func PermissionDenied(message string, a ...interface{}) error {
	return status.Errorf(codes.PermissionDenied, message, a...)
}

func ResourceExhausted(message string, a ...interface{}) error {
	return status.Errorf(codes.ResourceExhausted, message, a...)
}

func FailedPrecondition(message string, a ...interface{}) error {
	return status.Errorf(codes.FailedPrecondition, message, a...)
}

func Aborted(message string, a ...interface{}) error {
	return status.Errorf(codes.Aborted, message, a...)
}

func OutOfRange(message string, a ...interface{}) error {
	return status.Errorf(codes.OutOfRange, message, a...)
}

func Unimplemented(message string, a ...interface{}) error {
	return status.Errorf(codes.Unimplemented, message, a...)
}

func Internal(message string, a ...interface{}) error {
	return status.Errorf(codes.Internal, message, a...)
}

func Unavailable(message string, a ...interface{}) error {
	return status.Errorf(codes.Unavailable, message, a...)
}

func DataLoss(message string, a ...interface{}) error {
	return status.Errorf(codes.DataLoss, message, a...)
}

func Unauthenticated(message string, a ...interface{}) error {
	return status.Errorf(codes.Unauthenticated, message, a...)
}

# gRPC Server

Configurable gRPC server tools.

## Installation
```bash
go get -u bitbucket.org/labpatoscedro/grpc
```

## Usage
Setup server example:
```go
import (
	// ... 
	"bitbucket.org/labpatoscedro/people.conversations/internal/infrastructure/logger"
	"bitbucket.org/labpatoscedro/grpc/server"
	"google.golang.org/grpc"
	// ...
)

func NewGRPCServer(log logger.Logger, myServiceServer *v1.MyServiceServer) *server.Server {
	// instantiate grpc server
	svr := server.NewServer(
		// set service name
		server.WithName("my-service"),

		// set port to listen on
		server.WithPort(9000),

		// enable panic recovery
		WithRecovery(),

		// add logger
		WithLogger(log),
	)

	// add health check functions
	svr.AddHealthChecks(checks...)

	// register grpc servers
	svr.RegisterGRPCHandlersFunc(func(server *grpc.Server) {
		// register implemented grpc servers
		v1.RegisterMyServiceServer(server, myServiceServer)
	})
	
	return svr
}
```

Start server:
```go
panic(svr.Serve())
```

## GRPC Gateway
Install grpc-gateway
```shell
go get -u github.com/grpc-ecosystem/grpc-gateway/v2
```

Setup grpc-gateway to serve over HTTP
```go
import (
	"context"
	"fmt"
	"net/http"

	".../internal/config"
	".../internal/infrastructure/logger"
	".../pkg/v1"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/encoding/protojson"
)

type GRPCGateway interface {
	Serve() error
}

type gateway struct {
	ctx          context.Context
	cancel       context.CancelFunc
	logger       logger.Logger
	grpcEndpoint string
	httpEndpoint string
}

func NewGRPCGateway(container config.Container, log logger.Logger) GRPCGateway {
	ctx, cancel := context.WithCancel(context.Background())

	return &gateway{
		ctx:          ctx,
		cancel:       cancel,
		grpcEndpoint: fmt.Sprintf(":%d", container.GRPCPort()),
		httpEndpoint: fmt.Sprintf(":%d", container.HTTPPort()),
		logger:       log,
	}
}

func (g *gateway) Serve() error {
	defer g.cancel()

	mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
		MarshalOptions:   protojson.MarshalOptions{UseProtoNames: true},
		UnmarshalOptions: protojson.UnmarshalOptions{DiscardUnknown: true},
	}))

	opts := []grpc.DialOption{grpc.WithInsecure()}
	
	// register application handler 
	err := v1.RegisterMyServiceHandlerFromEndpoint(g.ctx, mux, g.grpcEndpoint, opts)
	if err != nil {
		return err
	}

	g.logger.Info("starting HTTP server", "addr", g.httpEndpoint)
	return http.ListenAndServe(g.httpEndpoint, mux)
}
```

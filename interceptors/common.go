package interceptors

import (
	"bitbucket.org/labpatoscedro/logger"
	"google.golang.org/grpc/codes"
)

var codeLogLevelMap = map[codes.Code]logger.Level{
	codes.OK:                 logger.InfoLevel,
	codes.Canceled:           logger.InfoLevel,
	codes.Unknown:            logger.ErrorLevel,
	codes.InvalidArgument:    logger.InfoLevel,
	codes.DeadlineExceeded:   logger.WarnLevel,
	codes.NotFound:           logger.InfoLevel,
	codes.AlreadyExists:      logger.InfoLevel,
	codes.PermissionDenied:   logger.WarnLevel,
	codes.Unauthenticated:    logger.InfoLevel,
	codes.ResourceExhausted:  logger.WarnLevel,
	codes.FailedPrecondition: logger.WarnLevel,
	codes.Aborted:            logger.WarnLevel,
	codes.OutOfRange:         logger.WarnLevel,
	codes.Unimplemented:      logger.ErrorLevel,
	codes.Internal:           logger.ErrorLevel,
	codes.Unavailable:        logger.WarnLevel,
	codes.DataLoss:           logger.ErrorLevel,
}

func getCodeLevel(code codes.Code) logger.Level {
	if level, ok := codeLogLevelMap[code]; ok {
		return level
	}

	return logger.ErrorLevel
}

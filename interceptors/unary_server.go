package interceptors

import (
	"context"
	"strings"
	"time"

	"bitbucket.org/labpatoscedro/logger"
	grpc_logging "github.com/grpc-ecosystem/go-grpc-middleware/logging"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc"
)

func UnaryServerRecovery() grpc.UnaryServerInterceptor {
	return grpc_recovery.UnaryServerInterceptor()
}

func UnaryServerLogger(l logger.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler) (interface{}, error) {
		startTime := time.Now()
		response, err := handler(ctx, req)
		logUnaryRequest(l, req, info, startTime, response, err)

		return response, err
	}
}

func logUnaryRequest(l logger.Logger,
	req interface{},
	info *grpc.UnaryServerInfo,
	startTime time.Time,
	response interface{}, err error) {
	duration := time.Since(startTime)
	code := grpc_logging.DefaultErrorToCode(err)
	values := strings.Split(info.FullMethod, "/")
	l.Log(getCodeLevel(code), "finished unary call with code "+code.String(),
		"grpc_service", values[1], "grpc_method", values[2], "grpc_code", code.String(),
		"grpc_request", req, "grpc_response", response, "grpc_start_time", startTime,
		"grpc_deadline", startTime.Add(duration), "grpc_duration", duration.String(),
	)
}

package server

import (
	"fmt"
	"net"

	healthv1 "bitbucket.org/labpatoscedro/grpc/health/v1"
	"bitbucket.org/labpatoscedro/logger"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func NewServer(options ...Option) *Server {
	server := &Server{port: 9091, log: logger.Nil()}

	for _, optionsFunc := range options {
		optionsFunc(server)
	}

	server.gRPCServer = grpc.NewServer(
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(server.unaryInterceptors...)),
		grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(server.streamInterceptors...)),
	)
	healthv1.RegisterHealthServer(server.gRPCServer, NewHealthServer(&server.healthChecks))

	return server
}

type Server struct {
	name               string
	port               int
	gRPCServer         *grpc.Server
	healthChecks       []func() error
	log                logger.Logger
	unaryInterceptors  []grpc.UnaryServerInterceptor
	streamInterceptors []grpc.StreamServerInterceptor
}

func (s *Server) EnableGRPCReflection() *Server {
	reflection.Register(s.gRPCServer)
	return s
}

func (s *Server) AddHealthChecks(checks ...func() error) *Server {
	s.healthChecks = append(s.healthChecks, checks...)
	return s
}

func (s *Server) RegisterGRPCHandlersFunc(registerFunc func(server *grpc.Server)) {
	registerFunc(s.gRPCServer)
}

func (s *Server) Serve() error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", s.port))
	if err != nil {
		s.log.Panic("failed to initialize listener", "err", err)
	}

	s.log.Info("serving gRPC server...", "server", s.name, "port", s.port)
	return s.gRPCServer.Serve(lis)
}

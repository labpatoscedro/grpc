package server

import (
	"context"
	"sync"
	"time"

	healthv1 "bitbucket.org/labpatoscedro/grpc/health/v1"
)

func NewHealthServer(checks *[]func() error) healthv1.HealthServer {
	return &HealthServer{
		checks: checks,
		mutex:  &sync.Mutex{},
	}
}

type HealthServer struct {
	checks        *[]func() error
	checkInterval time.Duration
	mutex         *sync.Mutex
	healthv1.UnimplementedHealthServer
}

func (h *HealthServer) Check(_ context.Context, _ *healthv1.HealthCheckRequest) (*healthv1.HealthCheckResponse, error) {
	err := h.runChecks()

	return &healthv1.HealthCheckResponse{Status: h.getHealthCheckStatus(err)}, err
}

func (h *HealthServer) Watch(_ *healthv1.HealthCheckRequest, watchServer healthv1.Health_WatchServer) error {
	for {
		if err := h.runStreamCheck(watchServer); err != nil {
			return err
		}

		time.Sleep(h.checkInterval)
	}
}

func (h *HealthServer) getHealthCheckStatus(err error) healthv1.HealthCheckResponse_ServingStatus {
	if err == nil {
		return healthv1.HealthCheckResponse_SERVING
	}

	return healthv1.HealthCheckResponse_NOT_SERVING
}

func (h *HealthServer) runStreamCheck(watchServer healthv1.Health_WatchServer) error {
	if err := h.runChecks(); err == nil {
		response := &healthv1.HealthCheckResponse{Status: h.getHealthCheckStatus(err)}

		if errSend := watchServer.Send(response); errSend != nil {
			return errSend
		}
	}

	return nil
}

func (h *HealthServer) runChecks() error {
	h.mutex.Lock()
	defer h.mutex.Unlock()

	if h.checks == nil || len(*h.checks) == 0 {
		return nil
	}

	for _, check := range *h.checks {
		if err := check(); err != nil {
			return err
		}
	}

	return nil
}

package server

import (
	"context"
	"errors"

	"bitbucket.org/labpatoscedro/grpc/interceptors"
	"bitbucket.org/labpatoscedro/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Option func(s *Server)

func WithName(name string) Option {
	return func(s *Server) {
		s.name = name
	}
}

func WithPort(port int) Option {
	return func(s *Server) {
		s.port = port
	}
}

func WithLogger(log logger.Logger) Option {
	return func(s *Server) {
		s.log = log
		s.unaryInterceptors = append(s.unaryInterceptors, interceptors.UnaryServerLogger(log))
		s.streamInterceptors = append(s.streamInterceptors, interceptors.StreamServerLogger(log))
	}
}

func WithRecovery() Option {
	return func(s *Server) {
		s.unaryInterceptors = append(s.unaryInterceptors, interceptors.UnaryServerRecovery())
		s.streamInterceptors = append(s.streamInterceptors, interceptors.StreamServerRecovery())
	}
}

func WithUnaryInterceptors(serverInterceptors ...grpc.UnaryServerInterceptor) Option {
	return func(s *Server) {
		s.unaryInterceptors = append(s.unaryInterceptors, serverInterceptors...)
	}
}

func WithStreamInterceptors(streamInterceptors ...grpc.StreamServerInterceptor) Option {
	return func(s *Server) {
		s.streamInterceptors = append(s.streamInterceptors, streamInterceptors...)
	}
}

//nolint:funlen // Cannot be reduced
func WithUnaryErrorInterceptor(errMap map[error]func(error) error) Option {
	return func(s *Server) {
		s.unaryInterceptors = append(s.unaryInterceptors, func(ctx context.Context,
			req interface{},
			info *grpc.UnaryServerInfo,
			handler grpc.UnaryHandler) (resp interface{}, err error) {
			if err == nil {
				return resp, nil
			}

			for targetErr, errHandler := range errMap {
				if errors.Is(err, targetErr) {
					return resp, errHandler(targetErr)
				}
			}

			s.log.Error("request failed due to internal error", "error", err)

			return resp, status.Errorf(codes.Internal, "internal server error")
		})
	}
}
